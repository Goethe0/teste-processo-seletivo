# Cadastro de Incidentes.

Implementar em PHP e com um framework a sua escolha um CRUD para realizar o cadastro de incidentes. Cada inicidente possui:

- ID único
- Um título (obrigatório)
- Uma descrição  (obrigatório)
- Criticidade (Alta, média ou baixa)
- Um tipo (obrigatório)
- Um status (aberto ou fechado). Todo incidente deverá ser cadastrado com o status aberto

## O tipos possíveis para incidentes são:
- Ataque Brute Force
- Credencias vazadas
- Ataque de DDoS
- Atividades anormais de usuários

Implemente apenas as telas necessárias para fazer o CRUD, não há necessidade de implementar um login, por exemplo.

## O que será avaliado?

- Qualidade de código
- Validação dos dados inseridos pelo usuário
- Cobertura de testes.